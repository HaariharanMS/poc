from fastapi import APIRouter, Depends
from typing import List, Optional
from pydantic import BaseModel
from .data_base import cnxn_string
import pyodbc
import time
import sys

router = APIRouter()

def json_preparation(Data):
    CategoryName_ = set() 
    CorporateBrandName_ = set() 
    BrandFormName_ = set() 
    SKU_ = set()
    ProductName_ = set()

    empty_list = []

    data_json = {}
    #print(type(Data))

    for data in Data:
            _SKUCode, _ProductName, _CategoryName, _CorporateBrandName,_BrandFormName = data
            
            SKU_.add((_SKUCode +"_" +_ProductName))
            CategoryName_.add(_CategoryName)
            CorporateBrandName_.add(_CorporateBrandName)
            BrandFormName_.add(_BrandFormName)

    if None in SKU_:
        SKU_.remove(None)
    if None in CategoryName_:
        CategoryName_.remove(None)
    if None in CorporateBrandName_:
        CorporateBrandName_.remove(None)
    if None in BrandFormName_:
        BrandFormName_.remove(None)

    SKUList = list(SKU_)
    ListBrandFormName_ = list(BrandFormName_)

    data_json = {
            "SKU":{"isLarge": len(SKUList) > 100,
                                "Count": len(SKUList),
                                "Data" : {"SKU":SKUList if len(SKUList) <= 100 else SKUList[:100]}
                            },
            "CategoryName":{"isLarge": len(CategoryName_) > 100,
                                "Count": len(CategoryName_),
                                "Data" : {"CategoryName":list(CategoryName_)}
                            },
            "CorporateBrandName":{"isLarge": len(CorporateBrandName_) > 100,
                                "Count": len(CorporateBrandName_),
                                "Data" : {"CorporateBrandName":list(CorporateBrandName_)}
                            },
            "BrandFormName":{"isLarge": len(BrandFormName_) > 100,
                                "Count": len(BrandFormName_),
                                "Data" : {"BrandFormName":ListBrandFormName_ if len(ListBrandFormName_) <= 100 else ListBrandFormName_[:100]}
                            }
                        }
    return data_json


# Initial Data for Dashboard (CategoryName, ClusterName, ProductFormName) 
@router.get('/default')
def read():
    start = time.time()
    Intial_data = None

    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()
        CategoryName_data = set()
        CorporateBrandName_data = set()
        BrandFormName_data = set()
        SKU_data = set()

        cursor.execute("SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy")
        Data = cursor.fetchall()
        con.close()

        for data in Data:
            _SKUCode, _ProductName, _Category, _CorporateBrandName, _BrandFormName = data
            CategoryName_data.add(_Category)
            CorporateBrandName_data.add(_CorporateBrandName)
            BrandFormName_data.add(_BrandFormName)
            SKU_data.add(_SKUCode + "_" + _ProductName)


        if None in CategoryName_data:
            CategoryName_data.remove(None)
        if None in CorporateBrandName_data:
            CorporateBrandName_data.remove(None)
        if None in BrandFormName_data:
            BrandFormName_data.remove(None)

        BrandFormNameList = list(BrandFormName_data)
        BrandFormNameList.sort()
        SKUList = list(SKU_data)
        SKUList.sort()

        SKU_json = {"isLarge": len(SKUList) > 100,
                                    "Count": len(SKUList),
                                    "Data" : {"SKU":SKUList if len(SKUList) <= 100 else SKUList[:100]}
                                }

        CategoryName_json = {"isLarge": len(CategoryName_data) > 100,
                                    "Count": len(CategoryName_data),
                                    "Data" : {"CategoryName":list(CategoryName_data)}
                                }

        CorporateBrandName_json = {"isLarge": len(CorporateBrandName_data) > 100,
                                    "Count": len(CorporateBrandName_data),
                                    "Data" : {"CorporateBrandName":list(CorporateBrandName_data)}
                                }

        BrandFormName_json = {"isLarge": len(BrandFormName_data) > 100,
                                    "Count": len(BrandFormName_data),
                                    "Data" : {"BrandFormName":BrandFormNameList if len(BrandFormNameList) <= 100 else BrandFormNameList[:100] }
                                }
        Plant_json = {"isLarge": 0 > 100,
                                    "Count": 0,
                                    "Data" : {"Plant":list()} 
                                    }

        ISOCountryName_json = {"isLarge": 0 > 100,
                                    "Count": 0,
                                    "Data" : {"ISOCountryName":list()} 
                                    }

        ISOCountryCode_json = {"isLarge": 0 > 100,
                                    "Count": 0,
                                    "Data" : {"ISOCountryCode":list()}
                                }

        

        Intial_data = {"SKU" : SKU_json, "CategoryName":CategoryName_json, "CorporateBrandName": CorporateBrandName_json,"BrandFormName" : BrandFormName_json, "Plant":Plant_json, "ISOCountryName":ISOCountryName_json, "ISOCountryCode":ISOCountryCode_json   }


    except Exception as e:
        data_json = {"Error message": e.__class__}

    end = time.time()
    print("time taken", end - start)
    return {"Data": Intial_data}

# Request body structure creation

class Product(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    BrandFormName: Optional[str] = None


class LoadMoreProduct(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    BrandFormName: Optional[str] = None
    NextTen: Optional[int] = 0

class LoadMoreBrandForm(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    NextRecord: Optional[int] = 0

class GetProduct(BaseModel):
    searchchar: Optional[str] = None

class LoadMoreSku(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    BrandFormName: Optional[str] = None
    NextRecord: int
    SearchChar: Optional[str] = None

class SearchSKU(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    BrandFormName: Optional[str] = None
    SearchChar : str  

class LoadMoreBrandFormName(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    BrandFormName: Optional[str] = None
    NextRecord: int
    SearchChar: Optional[str] = None

class SearchBrandFormName(BaseModel):
    CategoryName: Optional[str] = None
    CorporateBrandName: Optional[str] = None
    SearchChar : str


class PlantCenter(BaseModel):
    SKUcode : str

@router.post('/product')
def Product(product_name: Product):
    start = time.time()
    data_json = None

    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()

        if product_name.CategoryName != None and product_name.CorporateBrandName != None and product_name.BrandFormName != None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{product_name.CategoryName}' AND CorporateBrandName = '{product_name.CorporateBrandName}' AND BrandFormName = '{product_name.BrandFormName}'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)
        
        elif product_name.CategoryName == None and product_name.CorporateBrandName == None and product_name.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif product_name.CategoryName == None and product_name.CorporateBrandName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE BrandFormName = '{product_name.BrandFormName}'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif product_name.CorporateBrandName == None and product_name.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{product_name.CategoryName}'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif product_name.CategoryName == None and product_name.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CorporateBrandName = '{product_name.CorporateBrandName}'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)
        
        elif product_name.CategoryName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CorporateBrandName = '{product_name.CorporateBrandName}' AND BrandFormName = '{product_name.BrandFormName}'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif product_name.CorporateBrandName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{product_name.CategoryName}' AND BrandFormName = '{product_name.BrandFormName}'")    
            Data = cursor.fetchall()

            data_json = json_preparation(Data)
        
        elif product_name.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{product_name.CategoryName}' AND CorporateBrandName = '{product_name.CorporateBrandName}'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}

    end = time.time()
    print("Time Taken ", end - start)
    return data_json

def final_pro_load_more(LoadMoreBrandForm_):
    Next = []
    for data in LoadMoreBrandForm_:
        Next.append(data[0])
    data_json = {"BrandFormName": Next}
    return data_json 

def Searchbrandformname(Data):
    Search_brand_form = []
    for data in Data:
        Search_brand_form.append(data[0])

    data_json = {"isLarge": len(Search_brand_form) > 100,
                                "Count": len(Search_brand_form),
                                "Data" : {"BrandFormName":Search_brand_form if len(Search_brand_form) <= 100 else Search_brand_form[:100] }
                            }
    return {"BrandFormName": data_json}

@router.post("/searchbrandformname")
def search_brand_formname(search: SearchBrandFormName):
    data_json = None
    start = time.time()
    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()

        if search.CategoryName != None and search.CorporateBrandName != None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName FROM dbo.Product_Hierarchy WHERE CategoryName = '{search.CategoryName}' and CorporateBrandName = '{search.CorporateBrandName}' AND BrandFormName LIKE '%{search.SearchChar}%'")
            loadmoreBrandForm = cursor.fetchall()
            data_json = json_preparation(loadmoreBrandForm)

        elif search.CategoryName == None and search.CorporateBrandName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName FROM dbo.Product_Hierarchy WHERE BrandFormName LIKE '%{search.SearchChar}%'")
            loadmoreBrandForm = cursor.fetchall()
            data_json = json_preparation(loadmoreBrandForm)
        
        elif search.CorporateBrandName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName FROM dbo.Product_Hierarchy WHERE CategoryName = '{search.CategoryName}' AND BrandFormName LIKE '%{search.SearchChar}%'")
            loadmoreBrandForm = cursor.fetchall()
            data_json = json_preparation(loadmoreBrandForm)

        elif search.CategoryName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName FROM dbo.Product_Hierarchy WHERE CorporateBrandName= '{search.CorporateBrandName}' AND BrandFormName LIKE '%{search.SearchChar}%'")
            loadmoreBrandForm = cursor.fetchall()
            data_json = json_preparation(loadmoreBrandForm)
        
        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}

    end = time.time()
    print("Time Taken", end - start)

    return data_json


@router.post("/loadmorebrandformname")
def loadmore_default(loadmore: LoadMoreBrandFormName):
    start = time.time()
    data_json = None
    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()
        numberOfData = int(loadmore.NextRecord)
        default_num = 100

        if loadmore.SearchChar != None:
            if loadmore.CategoryName != None and loadmore.CorporateBrandName != None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and CorporateBrandName = '{loadmore.CorporateBrandName}' AND BrandFormName LIKE '%{loadmore.SearchChar}%' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)

            elif loadmore.CategoryName == None and loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE BrandFormName LIKE '%{loadmore.SearchChar}%' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)
            
            elif loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' AND BrandFormName LIKE '%{loadmore.SearchChar}%' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)

            elif loadmore.CategoryName == None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE CorporateBrandName= '{loadmore.CorporateBrandName}' AND BrandFormName LIKE '%{loadmore.SearchChar}%' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)

        else:
            if loadmore.CategoryName != None and loadmore.CorporateBrandName != None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and CorporateBrandName = '{loadmore.CorporateBrandName}' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)

            elif loadmore.CategoryName == None and loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)
            
            elif loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)

            elif loadmore.CategoryName == None:
                cursor.execute(f"SELECT DISTINCT BrandFormName FROM dbo.Product_Hierarchy WHERE CorporateBrandName= '{loadmore.CorporateBrandName}' ORDER BY BrandFormName OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                loadmoreBrandForm = cursor.fetchall()
                data_json = final_pro_load_more(loadmoreBrandForm)

        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}

    end = time.time()
    print("Time Taken", end - start)

    return data_json

def SKULoadMore(Data):
    SKU_ProductName = set()
    for data in Data:
        _SKU, _ProductName = data
        SKU_ProductName.add(_SKU + "_" + _ProductName)
    return list(SKU_ProductName)


@router.post('/loadmoresku')
def LoadMoreSKU(loadmore: LoadMoreSku):
    start = time.time()
    data_json = None
    try:
        numberOfData = int(loadmore.NextRecord)
        default_num = 100
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()
        if loadmore.SearchChar == None:
            if loadmore.CategoryName != None and loadmore.CorporateBrandName != None and loadmore.BrandFormName != None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and CorporateBrandName = '{loadmore.CorporateBrandName}' and BrandFormName = '{loadmore.BrandFormName}' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}

            elif loadmore.CategoryName == None and loadmore.CorporateBrandName == None and loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CategoryName == None and loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE BrandFormName = '{loadmore.BrandFormName}' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CategoryName == None and loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CorporateBrandName = '{loadmore.CorporateBrandName}' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CorporateBrandName == None and loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}'ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CategoryName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CorporateBrandName = '{loadmore.CorporateBrandName}' and BrandFormName = '{loadmore.BrandFormName}' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and BrandFormName = '{loadmore.BrandFormName}' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and CorporateBrandName = '{loadmore.CorporateBrandName}' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
        else:
            if loadmore.CategoryName != None and loadmore.CorporateBrandName != None and loadmore.BrandFormName != None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and CorporateBrandName = '{loadmore.CorporateBrandName}' and BrandFormName = '{loadmore.BrandFormName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}

            elif loadmore.CategoryName == None and loadmore.CorporateBrandName == None and loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CategoryName == None and loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE BrandFormName = '{loadmore.BrandFormName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CategoryName == None and loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CorporateBrandName = '{loadmore.CorporateBrandName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CorporateBrandName == None and loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CategoryName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CorporateBrandName = '{loadmore.CorporateBrandName}' and BrandFormName = '{loadmore.BrandFormName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.CorporateBrandName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and BrandFormName = '{loadmore.BrandFormName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}
            elif loadmore.BrandFormName == None:
                cursor.execute(f"SELECT DISTINCT SKUCode, ProductName FROM dbo.Product_Hierarchy WHERE CategoryName = '{loadmore.CategoryName}' and CorporateBrandName = '{loadmore.CorporateBrandName}' AND ProductName LIKE '%{loadmore.SearchChar}%' ORDER BY SKUCode OFFSET {numberOfData} ROWS FETCH NEXT {default_num} ROWS ONLY")
                Data = cursor.fetchall()
                SKU = SKULoadMore(Data)
                data_json = {"SKU": SKU}

        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}

    end = time.time()
    print("Time taken: ", end - start)
    return data_json

@router.post("/searchsku")
def searchSKU(search:SearchSKU):
    start = time.time()
    data_json = None
    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()

        if search.CategoryName != None and search.CorporateBrandName != None and search.BrandFormName != None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{search.CategoryName}' AND CorporateBrandName = '{search.CorporateBrandName}' AND BrandFormName = '{search.BrandFormName}' AND ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)
        
        elif search.CategoryName == None and search.CorporateBrandName == None and search.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()
            data_json = json_preparation(Data)

        elif search.CategoryName == None and search.CorporateBrandName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE BrandFormName = '{search.BrandFormName}'AND ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif search.CorporateBrandName == None and search.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{search.CategoryName}' AND ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif search.CategoryName == None and search.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CorporateBrandName = '{search.CorporateBrandName}' AND ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)
        
        elif search.CategoryName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CorporateBrandName = '{search.CorporateBrandName}' AND BrandFormName = '{search.BrandFormName}' AND ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        elif search.CorporateBrandName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{search.CategoryName}' AND BrandFormName = '{search.BrandFormName}' AND ProductName LIKE '%{search.SearchChar}%'")    
            Data = cursor.fetchall()

            data_json = json_preparation(Data)
        
        elif search.BrandFormName == None:
            cursor.execute(f"SELECT DISTINCT SKUCode, ProductName, CategoryName, CorporateBrandName, BrandFormName from dbo.Product_Hierarchy WHERE CategoryName = '{search.CategoryName}' AND CorporateBrandName = '{search.CorporateBrandName}' AND ProductName LIKE '%{search.SearchChar}%'")
            Data = cursor.fetchall()

            data_json = json_preparation(Data)

        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}

    end = time.time()
    print("Time Taken ", end - start)
    return data_json


@router.post('/distributioncenter')
def distributioncenter(Code:PlantCenter):
    start = time.time()
    data_json = None

    CategoryName_ = set() 
    CorporateBrandName_ = set() 
    BrandFormName_ = set() 
    SKU_ = set()
    ProductName_ = set()
    PlantName_ = set()

    sku = Code.SKUcode.split('_')[0]

    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()
        DestinationCenter = set()
        SKU = set()

        #skuCode = int(Code.SKUcode)
        cursor.execute(f"SELECT DISTINCT dbo.Product_Hierarchy.CategoryName, dbo.Product_Hierarchy.BrandFormName, dbo.Product_Hierarchy.SKUCode, dbo.Product_Hierarchy.SKURegionId, dbo.Product_Hierarchy.CorporateBrandName, dbo.Product_Hierarchy.ProductName, dbo.BoM_Explosion.PlantCode, dbo.Plant_csv.PlantName from ((dbo.Product_Hierarchy  INNER JOIN dbo.BoM_Explosion ON dbo.Product_Hierarchy.SKUCode=dbo.BoM_Explosion.FinishedGoodMaterialNumber)INNER JOIN dbo.Plant_csv ON dbo.BoM_Explosion.PlantCode=dbo.Plant_csv.PlantCode) WHERE dbo.Product_Hierarchy.SKUCode = '{sku}'")
            
        Data = cursor.fetchall()
       
        for data in Data:
            CategoryName_.add(data[0])
            CorporateBrandName_.add(data[4])
            BrandFormName_.add(data[1])
            SKU_.add(data[2]+"_"+data[5])
            PlantName_.add(data[6]+"/"+data[7])


            SKUList = list(SKU_)
            ListBrandFormName_ = list(BrandFormName_)

            SKU_json = {"isLarge": len(SKUList) > 100,
                                    "Count": len(SKUList),
                                    "Data" : {"SKU":SKUList if len(SKUList) <= 100 else SKUList[:100]}
                                }

            CategoryName_json = {"isLarge": len(CategoryName_) > 100,
                                    "Count": len(CategoryName_),
                                    "Data" : {"CategoryName":list(CategoryName_)}
                                }

            CorporateBrandName_json = {"isLarge": len(CorporateBrandName_) > 100,
                                    "Count": len(CorporateBrandName_),
                                    "Data" : {"CorporateBrandName":list(CorporateBrandName_)}
                                }

            BrandFormName_json = {"isLarge": len(BrandFormName_) > 100,
                                    "Count": len(BrandFormName_),
                                    "Data" : {"BrandFormName":ListBrandFormName_ if len(ListBrandFormName_) <= 100 else BrandFormNameList[:100] }
                                }
            PlantName_json = {"isLarge": len(PlantName_) > 100,
                                    "Count": len(PlantName_),
                                    "Data" : {"Plant":list(PlantName_) }
                                }
            
            data_json = {"SKU" : SKU_json, "CategoryName":CategoryName_json, "CorporateBrandName": CorporateBrandName_json,"BrandFormName" : BrandFormName_json, "Plant":PlantName_json}
                         
        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}

    return data_json
class DestinationCountry(BaseModel):
    PlantCode : str
    PlantName : str

@router.post('/destinationcountry')
def Destinationcountry(Destination:DestinationCountry):
    data_json = None
    start = time.time()
    try:
        con = pyodbc.connect(cnxn_string)
        cursor = con.cursor()
        cursor.execute(f"SELECT DISTINCT dbo.HierarchyGeography_csv.ISOCountryName,\
            dbo.HierarchyGeography_csv.ISOCountryCode\
            FROM dbo.HierarchyGeography_csv\
            INNER JOIN dbo.Plant_csv ON  dbo.Plant_csv.CountryCode = dbo.HierarchyGeography_csv.ISOCountryCode  WHERE dbo.Plant_csv.PlantCode = '{Destination.PlantCode}' and dbo.Plant_csv.PlantName = '{Destination.PlantName}'")

        Data = cursor.fetchall()

        ISOCountryName_, ISOCountryCode_ = Data[0]

        data_json = {"ISOCountryName":ISOCountryName_,
                    "ISOCountryCode":ISOCountryCode_}

        con.close()

    except Exception as e:
        data_json = {"Error message": e.__class__}


    end = time.time()

    print("Time Taken", end - start)

    return data_json
