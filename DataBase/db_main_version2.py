# from fastapi import APIRouter, Depends
# from .data_base import cnxn_string
# import models 
# import pyodbc
# import time
# import sys

# router = APIRouter()

# @router.get('/default/v1')
# def default():
#     start = time.time()
#     data_json = None

#     try:
#         con = pyodbc.connect(cnxn_string)
#         cursor = con.cursor()
#         CategoryNameData = set()
#         CorporateBrandNameData = set()
#         BrandFormNameData = set()
#         SKUData = set()

#         cursor.execute("""SELECT [CategoryName],[CorporateBrandName],[BrandFormName],[SKUCode],[SKURegionId],[ProductName] FROM [dbo].[A_stage_Active_SKU_Memory_Management_Test]""")
#         Data = cursor.fetchall()

#         for data in Data:
#             category_name, corporate_brand_name, brand_form_name, sku_code, sku_region_id, product_name = data
#             CategoryNameData.add(category_name)
#             CorporateBrandNameData.add(corporate_brand_name)
#             BrandFormNameData.add(brand_form_name)
#             SKUData.add(product_name+'/'+sku_code +'/'+ sku_region_id)

#         BrandFormNameList = list(BrandFormNameData)
#         BrandFormNameList.sort()
#         SKUList = list(SKUData)
#         SKUList.sort()

#         SKU_json = {"isLarge": len(SKUList) > 100,
#                                     "Count": len(SKUList),
#                                     "Data" : {"SKU":SKUList if len(SKUList) <= 100 else SKUList[:100]}
#                                 }
#         CategoryName_json = {"isLarge": len(CategoryNameData) > 100,
#                                     "Count": len(CategoryNameData),
#                                     "Data" : {"CategoryName":list(CategoryNameData)}
#                                 }
#         CorporateBrandName_json = {"isLarge": len(CorporateBrandNameData) > 100,
#                                     "Count": len(CorporateBrandNameData),
#                                     "Data" : {"CorporateBrandName":list(CorporateBrandNameData)}
#                                 }
#         BrandFormName_json = {"isLarge": len(BrandFormNameList) > 100,
#                                     "Count": len(BrandFormNameList),
#                                     "Data" : {"BrandFormName":BrandFormNameList if len(BrandFormNameList) <= 100 else BrandFormNameList[:100] }
#                                 }

#         data_json = {"SKU" : SKU_json, "CategoryName":CategoryName_json, "CorporateBrandName": CorporateBrandName_json,"BrandFormName" : BrandFormName_json, "Plant":{"Data":list()}, "ISOCountryName":{"Data":list()}, "ISOCountryCode":{"Data":list()}}
    
#         con.close()

#     except Exception as e:
#         data_json = {"Error message":e.__class__}
#     end = time.time()
#     print("Time Taken :", end - start)

#     return data_json


# @router.post('/Product/v1')
# def Product(ProductColumn: models.ProductRequestBody):
#     print(ProductColumn)