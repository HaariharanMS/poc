from fastapi import APIRouter
import pyodbd
from .data_base import cnxn_string

router = APIRouter()

@router.get('/default')
def read_default():
    con = pyodbc.connect(cnxn_string)
    cursor = con.cursor()

    CategoryGroupname_data = []
    ClusterName_data = []
    ProductFormName_data = []


    con.close()