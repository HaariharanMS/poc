from fastapi import FastAPI
from DataBase import db_main, db_main_version2
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(db_main.router)
#app.include_router(db_main_version2.router)