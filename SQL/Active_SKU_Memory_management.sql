select distinct SUBSTRING(DivisionName,1,30) AS DivisionName,	
SUBSTRING(CategoryGroupName,1,30) AS SUBSTRING,	
SUBSTRING(CategoryName,1,50) AS CategoryName,	
SUBSTRING(CorporateBrandName,1,30) AS CorporateBrandName,
SUBSTRING(BrandFormName,1,30) AS BrandFormName,	
SUBSTRING(SKUCode,1,30) AS SKUCode,	
SUBSTRING(SKURegionId,1,30) AS SKURegionId,	
SUBSTRING(ProductName,1,30) AS ProductName

into A_stage_Active_SKU_Memory_Management_Test

FROM [dbo].[Product_Hierarchy] as a
inner join [dbo].[MRDR] as b
on a.SKUCode=b.[MRDR_SKU]